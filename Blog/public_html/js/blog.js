
$(function () {
    var APPLICATION_ID = "F48E22C8-E8E2-7271-FFB1-A13710432A00",
        SECRET_KEY = "06EFC679-5BC2-CEF0-FF44-2023B94CC000",
        VERSION = "v1";
        
        Backendless.initApp(APPLICATION_ID, SECRET_KEY, VERSION);
        
        var postsCollection = Backendless.Persistence.of(Posts).find();
        
        console.log(postsCollection);
        
        var wrapper = {
            
            posts: postsCollection.data
        };
        
        Handlebars.registerHelper('format', function(time) {
            return moment(time).format("dddd, MMMM Do YYYY")
            });
        
        
        var blogScript = $("#blogs-template").html();
        var blogTemplate = Handlebars.compile(blogScript);
        var blogHTML = blogTemplate(wrapper);
        
        $('.main-container').html(blogHTML);
        
            
});

   function Posts(args){
       args = args || "";
       this.title = args.title || "";
       this.content = args.content || "";
       this.authorEmail = args.authorEmail || "";
   }
   